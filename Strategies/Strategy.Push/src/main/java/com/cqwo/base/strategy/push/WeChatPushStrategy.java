package com.cqwo.base.strategy.push;

import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.data.rdbs.repository.push.WeChatPushRepository;
import com.cqwo.base.core.domain.appconfig.AppConfigInfo;
import com.cqwo.base.core.domain.push.WeChatPushRecordInfo;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.core.push.IWeChatPushStrategy;
import com.cqwo.base.strategy.config.AppConfigStrategy;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

@Component(value = "WeChatPushStrategy")
public class WeChatPushStrategy implements IWeChatPushStrategy {

    @Autowired
    private WeChatPushRepository weChatPushRepository;

    @Override
    public String sendPush(String appId, String openId, String templateId, String url, String miniprogram, String data) {

        JSONObject retJson = new JSONObject();
        retJson.put("status", ApiCollect.ERROR);

        AppConfigInfo appConfigInfo = AppConfigStrategy.appConfigMap.get(appId);
        if (appConfigInfo == null) {
            retJson.put("msg", "parameter appId is invalid");
            return retJson.toJSONString();
        }

        //1，配置
        /*WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
        wxStorage.setAppId(appConfigInfo.getAppKey());//appid
        wxStorage.setSecret(appConfigInfo.getAppSecret());//appsecret
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxStorage);

        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId(templateId)
                .build();
        if (!StringUtils.isEmpty(url))
            templateMessage.setUrl(url);
        if (!StringUtils.isEmpty(miniprogram)) {
            JSONObject jsonMiniprogram = JSONObject.parseObject(miniprogram);
            WxMpTemplateMessage.MiniProgram wxMiniProgram = templateMessage.getMiniProgram();
            wxMiniProgram.setAppid(jsonMiniprogram.getString("appid"));
            wxMiniProgram.setPagePath(jsonMiniprogram.getString("pagepath"));
            templateMessage.setMiniProgram(wxMiniProgram);
        }
        if (!StringUtils.isEmpty(data)) {
            JSONObject jsonData = JSONObject.parseObject(data);
            Iterator iter = jsonData.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                templateMessage.addData(new WxMpTemplateData(entry.getKey().toString(),
                        jsonData.getJSONObject(entry.getKey().toString()).getString("value"),
                        jsonData.getJSONObject(entry.getKey().toString()).getString("color")));
            }
        }*/
        WeChatPushRecordInfo weChatPushRecordInfo = new WeChatPushRecordInfo();
        weChatPushRecordInfo.setId(UUID.randomUUID().toString().replace("-", ""));
        weChatPushRecordInfo.setAppId(appId);
        weChatPushRecordInfo.setOpenId(openId);
        weChatPushRecordInfo.setTemplateId(templateId);
        weChatPushRecordInfo.setUrl(url);
        weChatPushRecordInfo.setMiniprogram(miniprogram);
        weChatPushRecordInfo.setData(data);
        weChatPushRecordInfo.setStatus(ApiCollect.ERROR);
        //发起推送
        try {
            //String ret = send(appId, openId, templateId, url, miniprogram, data, appConfigInfo);
            String ret = null;
            JSONObject wxPushRetJson = JSONObject.parseObject(ret);
            weChatPushRecordInfo.setErrCode(wxPushRetJson.getIntValue("errcode"));
            weChatPushRecordInfo.setErrMsg(wxPushRetJson.getString("errmsg"));
            weChatPushRecordInfo.setMsgId(wxPushRetJson.getString("msgid"));
            if (wxPushRetJson.getIntValue("errcode") == 0) {
                weChatPushRecordInfo.setStatus(ApiCollect.SUCCESS);
                retJson.put("status", ApiCollect.SUCCESS);
            } else {
                retJson.put("errcode", wxPushRetJson.getIntValue("errcode"));
                retJson.put("errmsg", wxPushRetJson.getString("errmsg"));
            }
            weChatPushRepository.save(weChatPushRecordInfo);
            retJson.put("wechatPushId", weChatPushRecordInfo.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retJson.toJSONString();
    }

    /*public static String send (String appId, String openId, String templateId, String url, String miniprogram, String data,
                             AppConfigInfo appConfigInfo) throws Exception {

        //1，配置
        WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
        wxStorage.setAppId(appConfigInfo.getAppKey());//appid
        wxStorage.setSecret(appConfigInfo.getAppSecret());//appsecret
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxStorage);

        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId(templateId)
                .build();
        if (!StringUtils.isEmpty(url))
            templateMessage.setUrl(url);
        if (!StringUtils.isEmpty(miniprogram)) {
            JSONObject jsonMiniprogram = JSONObject.parseObject(miniprogram);
            WxMpTemplateMessage.MiniProgram wxMiniProgram = templateMessage.getMiniProgram();
            wxMiniProgram.setAppid(jsonMiniprogram.getString("appid"));
            wxMiniProgram.setPagePath(jsonMiniprogram.getString("pagepath"));
            templateMessage.setMiniProgram(wxMiniProgram);
        }
        if (!StringUtils.isEmpty(data)) {
            JSONObject jsonData = JSONObject.parseObject(data);
            Iterator iter = jsonData.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                templateMessage.addData(new WxMpTemplateData(entry.getKey().toString(),
                        jsonData.getJSONObject(entry.getKey().toString()).getString("value"),
                        jsonData.getJSONObject(entry.getKey().toString()).getString("color")));
            }
        }
        return wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
    }*/
}
