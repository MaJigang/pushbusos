package com.cqwo.base.strategy.push.mobile.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.domain.appconfig.AppConfigInfo;
import com.cqwo.base.core.domain.push.MobilePushRecordInfo;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.strategy.config.AppConfigStrategy;

import java.util.*;

public class PushUtil {

    private static final Integer ALIYUN_APP_TYPE = 1;
    private static final Integer JIGUANG_APP_TYPE = 2;
    private static final Integer UMENG_APP_TYPE = 3;
    private static final Integer MOB_APP_TYPE = 4;

    public static MobilePushRecordInfo sendPush (MobilePushRecordInfo mobilePushRecordInfo) {
        JSONObject jsonUid = JSONObject.parseObject(mobilePushRecordInfo.getUid());
        Map<String, List<AppConfigInfo>> appsMap = AppConfigStrategy.getAppKeyAndSecret(mobilePushRecordInfo.getPackageName());
        // 初始化推送状态
        Integer status = ApiCollect.ERROR;

        Set<Integer> appTypes = new HashSet<>();
        for(Map.Entry<String, List<AppConfigInfo>> entry : appsMap.entrySet()){
            List<AppConfigInfo> appConfigInfos = entry.getValue();
            if (!appConfigInfos.isEmpty())
                appTypes.add(appConfigInfos.get(0).getAppType());
        }

        // aliyun push start
        if (appTypes.contains(ALIYUN_APP_TYPE)) {
            List<AppConfigInfo> apps = appsMap.get(mobilePushRecordInfo.getPackageName() + "-" + ALIYUN_APP_TYPE);
            JSONArray retValue = new JSONArray();
            for (AppConfigInfo appConfigInfo:apps) {
                String aUid = jsonUid.getString("apush");
                Long apushStartTime = System.currentTimeMillis();
                JSONObject apushRetJson = new AliyunPushUtil().sendAliyunPush(mobilePushRecordInfo.getTargetType(), aUid,
                        mobilePushRecordInfo.getType(), mobilePushRecordInfo.getTitle(), mobilePushRecordInfo.getContent(),
                        Long.valueOf(appConfigInfo.getAppKey()));
                Long time = System.currentTimeMillis() - apushStartTime;
                System.out.println("jpush time : [" + time + "]");
                if (!apushRetJson.containsKey("ErrCode"))
                    status = ApiCollect.SUCCESS;
                retValue.add(apushRetJson);
            }
            mobilePushRecordInfo.setApushRetValue(JSONObject.toJSONString(retValue));
        }
        // aliyun push end

        // j push start
        if (appTypes.contains(JIGUANG_APP_TYPE)) {
            List<AppConfigInfo> apps = appsMap.get(mobilePushRecordInfo.getPackageName() + "-" + JIGUANG_APP_TYPE);
            JSONArray retValue = new JSONArray();
            for (AppConfigInfo appConfigInfo:apps) {
                String jUid = jsonUid.getString("jpush");
                JSONObject jpushRetJson = JpushUtil.sendJpush(mobilePushRecordInfo.getTargetType(), jUid,
                        mobilePushRecordInfo.getType(), mobilePushRecordInfo.getTitle(), mobilePushRecordInfo.getContent(),
                        appConfigInfo.getAppKey(), appConfigInfo.getAppSecret());
                if (jpushRetJson.getIntValue("statusCode") == 200)
                    status = ApiCollect.SUCCESS;
                retValue.add(jpushRetJson);
            }
            mobilePushRecordInfo.setJpushRetValue(JSONObject.toJSONString(retValue));
        }
        // j push end

        // umeng push start
        if (appTypes.contains(UMENG_APP_TYPE)) {
            List<AppConfigInfo> apps = appsMap.get(mobilePushRecordInfo.getPackageName() + "-" + UMENG_APP_TYPE);
            JSONArray retValue = new JSONArray();
            for (AppConfigInfo appConfigInfo:apps) {
                String uUid = jsonUid.getString("upush");
                JSONObject upushRetJson = UMengUtil.sendUmengPush(mobilePushRecordInfo.getTargetType(), uUid,
                        mobilePushRecordInfo.getType(), mobilePushRecordInfo.getTitle(), mobilePushRecordInfo.getContent(),
                        appConfigInfo.getAppKey(), appConfigInfo.getAppSecret());
                if ("SUCCESS".equals(upushRetJson.getString("ret")))
                    status = ApiCollect.SUCCESS;
                retValue.add(upushRetJson);
            }
            mobilePushRecordInfo.setUpushRetValue(JSONObject.toJSONString(retValue));
        }
        // umeng push end

        // mob push start
        if (appTypes.contains(MOB_APP_TYPE)) {
            List<AppConfigInfo> apps = appsMap.get(mobilePushRecordInfo.getPackageName() + "-" + MOB_APP_TYPE);
            JSONArray retValue = new JSONArray();
            for (AppConfigInfo appConfigInfo:apps) {
                String mUid = jsonUid.getString("mpush");
                Long mpushStartTime = System.currentTimeMillis();
                JSONObject mpushRetJson = MobPushUtil.sendMobPush(mobilePushRecordInfo.getTargetType(), mUid,
                        mobilePushRecordInfo.getType(), mobilePushRecordInfo.getTitle(), mobilePushRecordInfo.getContent(),
                        appConfigInfo.getAppKey(), appConfigInfo.getAppSecret());
                Long time = System.currentTimeMillis() - mpushStartTime;
                System.out.println("mpush time : [" + time + "]");
                if (mpushRetJson.getIntValue("status") == 200)
                    status = ApiCollect.SUCCESS;
                retValue.add(mpushRetJson);
            }
            mobilePushRecordInfo.setMpushRetValue(JSONObject.toJSONString(retValue));
        }
        // mob push end

        mobilePushRecordInfo.setStatus(status);
        return mobilePushRecordInfo;
    }



}
