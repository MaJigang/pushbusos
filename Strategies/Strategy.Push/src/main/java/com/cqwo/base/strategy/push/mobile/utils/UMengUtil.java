package com.cqwo.base.strategy.push.mobile.utils;

import com.alibaba.fastjson.JSONObject;
import com.umeng.push.AndroidNotification;
import com.umeng.push.BaseUmengNotification;
import com.umeng.push.android.AndroidBroadcast;
import com.umeng.push.android.AndroidUnicast;
import com.umeng.push.helper.HttpHelper;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class UMengUtil {

    private static final String HOST = "http://msg.umeng.com";

    private static final String POST_PATH = "/api/send";

    public static JSONObject sendUmengPush (Integer targetType, String uid, Integer type, String title, String content,
                                            String appkey, String appSecret) {

        if (targetType == 1) {
            return new UMengUtil().sendAnroidBroadcast(title, content, appkey, appSecret);
        } else if (targetType == 2) {
            String displayType = null;
            if (type == 1) {
                displayType = "notification";
            } else {
                displayType = "message";
            }
            if (uid.contains(",")) {
                return new UMengUtil().sendAnroidToDeviceTokens(uid, displayType, title, content, appkey, appSecret);
            } else {
                return new UMengUtil().sendAnroidToDeviceToken(uid, displayType, title, content, appkey, appSecret);
            }
        }
        JSONObject jsonRet = new JSONObject();
        jsonRet.put("status", "500");
        jsonRet.put("error", "推送服务错误");
        return jsonRet;
    }

    /**
     * 发送推送Android单个设备
     *
     * @param deviceToken 设备
     * @param displayType 消息类型: notification(通知)、message(消息)
     * @param title       通知标题
     * @param text        通知内容
     * @returns
     * @throws IOException
     */
    private JSONObject sendAnroidToDeviceToken(String deviceToken, String displayType, String title, String text, String appkey, String appSecret) {
        AndroidUnicast unicast = null;
        try {
            unicast = new AndroidUnicast(appkey, appSecret);
            unicast.setDeviceToken(deviceToken);

            if ("notification".equals(displayType)) {
                unicast.setTicker(title);
                unicast.setTitle(title);
                unicast.setText(text);
                unicast.goAppAfterOpen();
                unicast.setProductionMode();
                unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            } else if ("message".equals(displayType)) {
                unicast.setDisplayType(AndroidNotification.DisplayType.MESSAGE);
                unicast.setCustomField(text);
            }

            return send(unicast, appSecret);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonRet = new JSONObject();
        jsonRet.put("status", "500");
        jsonRet.put("error", "推送服务错误");
        return jsonRet;
    }

    /**
     * 发送推送Android多个设备
     *
     * @param deviceTokens 设备
     * @param displayType 消息类型: notification(通知)、message(消息)
     * @param title       通知标题
     * @param text        通知内容
     * @returns
     * @throws IOException
     */
    private JSONObject sendAnroidToDeviceTokens(String deviceTokens, String displayType, String title, String text, String appkey, String appSecret) {
        AndroidListcast listcast = null;
        try {
            listcast = new AndroidListcast(appkey, appSecret);
            listcast.setDeviceTokens(deviceTokens);

            if ("notification".equals(displayType)) {
                listcast.setTicker(title);
                listcast.setTitle(title);
                listcast.setText(text);
                listcast.goAppAfterOpen();
                listcast.setProductionMode();
                listcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            } else if ("message".equals(displayType)) {
                listcast.setDisplayType(AndroidNotification.DisplayType.MESSAGE);
                listcast.setCustomField(text);
            }

            System.out.println("unicast:" + JSONObject.toJSONString(listcast));

            return send(listcast, appSecret);

        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonRet = new JSONObject();
        jsonRet.put("status", "500");
        jsonRet.put("error", "推送服务错误");
        return jsonRet;
    }

    /**
     * 发送Android广播
     *
     * @param title 通知标题
     * @param text  通知内容
     * @return
     * @throws IOException
     */
    private JSONObject sendAnroidBroadcast(String title, String text, String appkey, String appSecret) {
        AndroidBroadcast broadcast = null;
        try {
            broadcast = new AndroidBroadcast(appkey, appSecret);
            broadcast.setTicker(title);
            broadcast.setTitle(text);
            broadcast.setText(text);
            broadcast.goAppAfterOpen();
            broadcast.setProductionMode();
            broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);

            return send(broadcast, appSecret);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonRet = new JSONObject();
        jsonRet.put("status", "500");
        jsonRet.put("error", "推送服务错误");
        return jsonRet;
    }

    private static JSONObject send(BaseUmengNotification msg, String appSecret) throws Exception {
        String timestamp = Integer.toString((int)(System.currentTimeMillis() / 1000L));
        msg.setPredefinedKeyValue("timestamp", timestamp);
        String url = HOST + POST_PATH;
        String postBody = msg.getPostBody();
        String sign = DigestUtils.md5Hex(("POST" + url + postBody + appSecret).getBytes(StandardCharsets.UTF_8));
        url = url + "?sign=" + sign;
        String ret = HttpHelper.getInstance().post(url, postBody);
        return JSONObject.parseObject(ret);
    }

    class AndroidListcast extends AndroidNotification {

        public AndroidListcast(String appkey,String appMasterSecret) throws Exception {
            setAppMasterSecret(appMasterSecret);
            setPredefinedKeyValue("appkey", appkey);
            this.setPredefinedKeyValue("type", "listcast");
        }

        public void setDeviceTokens(String token) throws Exception {
            setPredefinedKeyValue("device_tokens", token);
        }

    }

}

