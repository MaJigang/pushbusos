package com.cqwo.base.strategy.push;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.data.rdbs.repository.push.MobilePushRepository;
import com.cqwo.base.core.domain.push.MobilePushRecordInfo;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.core.push.IMobilePushStrategy;
import com.cqwo.base.strategy.push.mobile.utils.PushUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(value = "MobilePushStrategy")
public class MobilePushStrategy implements IMobilePushStrategy {

    @Autowired
    private MobilePushRepository mobilePushRepository;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String sendPush(String uid, String appId, Integer targetType, Integer type, String title, String content,
                           String packageName) {
        MobilePushRecordInfo mobilePushRecordInfo = new MobilePushRecordInfo();
        mobilePushRecordInfo.setId(UUID.randomUUID().toString().replace("-",""));
        mobilePushRecordInfo.setUid(uid);
        mobilePushRecordInfo.setAppId(appId);
        mobilePushRecordInfo.setTargetType(targetType);
        mobilePushRecordInfo.setType(type);
        mobilePushRecordInfo.setTitle(title);
        mobilePushRecordInfo.setContent(content);
        mobilePushRecordInfo.setPackageName(packageName);
        mobilePushRecordInfo = PushUtil.sendPush(mobilePushRecordInfo);
        mobilePushRepository.save(mobilePushRecordInfo);
        JSONObject retJson = new JSONObject();
        retJson.put("pushId", mobilePushRecordInfo.getId());
        retJson.put("status", mobilePushRecordInfo.getStatus());
        return retJson.toJSONString();
    }

    @Override
    public String selPushByIds (String pushIds) {
        String[] pushIdArr = pushIds.split(",");
        List<MobilePushRecordInfo> mobilePushRecordInfos = mobilePushRepository.findAllById(Arrays.asList(pushIdArr));
        JSONObject retJson = new JSONObject();
        retJson.put("status", ApiCollect.SUCCESS);
        JSONArray pushInfosJson = new JSONArray();
        for (MobilePushRecordInfo m : mobilePushRecordInfos) {
            pushInfosJson.add(m);
        }
        retJson.put("pushInfo", pushInfosJson);
        return retJson.toJSONString();
    }

}
