package com.cqwo.base.strategy.push.mobile.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.push.model.v20160801.PushRequest;
import com.aliyuncs.push.model.v20160801.PushResponse;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AliyunPushUtil {

    // 此处为阿里云用户accessKeyId
    private static String accessKeyId = "LTAIuLRucQpGe1JV";
    // 此处为阿里云用户accessSecret
    private static String accessSecret = "Vq0Xa1JOrTKwgi8yaoYcMnjiekuEWc";

    //private static DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);

    public JSONObject sendAliyunPush (Integer targetType, String uid, Integer type,
                                          String title, String content, Long appKey) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
        IAcsClient client = new DefaultAcsClient(profile);
        PushRequest request = new PushRequest();
        request.setRegionId("cn-hangzhou");
        request.setAppKey(appKey);
        request.setDeviceType("ALL");
        if (targetType == 1)
            request.setTarget("ALL");
        else if (targetType == 2)
            request.setTarget("DEVICE");
        if (type == 1)
            request.setPushType("NOTICE");
        else if (type == 2)
            request.setPushType("MESSAGE");
        request.setTargetValue(uid);
        request.setBody(content);
        request.setTitle(title);
        try {
            PushResponse response = client.getAcsResponse(request);
            return JSON.parseObject(new Gson().toJson(response));
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            JSONObject jsonRet = new JSONObject();
            jsonRet.put("ErrCode", e.getErrCode());
            jsonRet.put("ErrMsg", e.getErrMsg());
            jsonRet.put("RequestId", e.getRequestId());
            return jsonRet;
        }
        JSONObject jsonRet = new JSONObject();
        jsonRet.put("ErrCode", "500");
        jsonRet.put("ErrMsg", "推送服务错误");
        return jsonRet;
    }

}
