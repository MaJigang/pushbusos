package com.cqwo.base.strategy.push.mobile.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import mob.push.api.exception.ApiException;
import mob.push.api.model.PushWork;
import mob.push.api.utils.HttpUtils;
import mob.push.api.utils.MobPushResult;

public class MobPushUtil {

    public static JSONObject sendMobPush (Integer targetType, String uid, Integer type, String title, String content,
                                          String appkey, String appSecret) {
        String ret = null;
        PushWork pushWork = new PushWork();
        pushWork.setAppkey(appkey);
        pushWork.setPlats(new Integer[]{1,2}); //可使用平台，1、android ； 2、ios ；如包含ios和android则为[1,2]
        pushWork.setTarget(targetType);
        pushWork.setRegistrationIds(uid.split(","));
        pushWork.setType(type);
        pushWork.setContent(content);
        pushWork.setAndroidTitle(title);
        pushWork.setIosTitle(title);
        try {
            ret = sendPush(pushWork, appkey, appSecret);
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject jsonRet = new JSONObject();
            jsonRet.put("status", "500");
            jsonRet.put("error", "推送服务错误");
            return jsonRet;
        }
        return JSONObject.parseObject(ret);
    }


    public static String sendPush(PushWork pushWork, String appkey, String appSecret) throws ApiException {
        String path = "http://api.push.mob.com/push";
        if (pushWork.getAppkey() == null) {
            pushWork.setAppkey(appkey);
        }
        if (pushWork.getContent() == null) {
            throw new ApiException(400, -1, "content is null");
        } else if (pushWork.getTarget() == null) {
            throw new ApiException(400, -1, "target is null");
        } else if (pushWork.getType() == null) {
            throw new ApiException(400, -1, "type is null");
        } else {
            HttpUtils.PostEntity entity = (new HttpUtils.PostEntity(path, appkey, appSecret, JSON.toJSONString(pushWork))).invoke(false);
            MobPushResult result = null;
            if (entity.getStatusCode() == 200) {
                result = JSON.toJavaObject(JSON.parseObject(entity.getResp()), MobPushResult.class);
                if (result != null) {
                    /*if (result.getRes() == null) {
                        return null;
                    } else {
                        JSONObject json = (JSONObject)result.getRes();
                        return json.getString("batchId");
                    }*/
                    return JSONObject.toJSONString(result);
                } else {
                    return null;
                }
            } else {
                result = JSON.toJavaObject(JSON.parseObject(entity.getResp()), MobPushResult.class);
                //throw new ApiException(entity.getStatusCode(), result.getStatus(), result.getError());
                return JSONObject.toJSONString(result);
            }
        }
    }

}
