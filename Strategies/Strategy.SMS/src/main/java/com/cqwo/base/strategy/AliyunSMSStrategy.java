package com.cqwo.base.strategy;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.cqwo.base.core.data.rdbs.repository.sms.AliyunSMSRepository;
import com.cqwo.base.core.domain.sms.AliyunSMSRecordInfo;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.core.sms.IAliyunSMSStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@Component(value = "AliyunSMSStrategy")
@Primary
public class AliyunSMSStrategy implements IAliyunSMSStrategy {

    @Autowired
    private AliyunSMSRepository aliyunSMSRepository;


    private String accessKeyId;

    private String accessSecret;

    @Override
    public String sendSms(String phoneNumbers, String signName, String templateCode, String templateParam) {

        JSONObject retJson = new JSONObject();
        retJson.put("status", ApiCollect.ERROR);
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessSecret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        // 参数
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", templateParam);
        try {
            CommonResponse response = client.getCommonResponse(request);
            JSONObject retJsonData = JSONObject.parseObject(response.getData());
            AliyunSMSRecordInfo aliyunSMSInfo = new AliyunSMSRecordInfo();
            aliyunSMSInfo.setId(UUID.randomUUID().toString().replace("-", ""));
            aliyunSMSInfo.setPhoneNumbers(phoneNumbers);
            aliyunSMSInfo.setSignName(signName);
            aliyunSMSInfo.setTemplateCode(templateCode);
            aliyunSMSInfo.setTemplateParam(templateParam);
            aliyunSMSInfo.setCode(retJsonData.getString("Code"));
            aliyunSMSInfo.setBizId(retJsonData.getString("BizId"));
            aliyunSMSInfo.setMessage(retJsonData.getString("Message"));
            aliyunSMSInfo.setRequestId(retJsonData.getString("RequestId"));
            if ("ok".equals(retJsonData.getString("Code"))) {
                aliyunSMSInfo.setStatus(ApiCollect.SUCCESS);
                retJson.put("status", ApiCollect.SUCCESS);
                retJson.put("smsId", aliyunSMSInfo.getId());
            } else {
                aliyunSMSInfo.setStatus(ApiCollect.ERROR);
                retJson.put("smsId", aliyunSMSInfo.getId());
                retJson.put("msg", response.getData());
            }
            aliyunSMSRepository.save(aliyunSMSInfo);
            return retJson.toJSONString();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return retJson.toJSONString();
    }

    @Override
    public String querySmsDetails(String smsIds) {
        JSONObject retJson = new JSONObject();
        retJson.put("status", ApiCollect.ERROR);
        String[] idArr = smsIds.split(",");
        List<AliyunSMSRecordInfo> aliyunSMSInfos = aliyunSMSRepository.findAllById(Arrays.asList(idArr));
        if (aliyunSMSInfos.isEmpty())
            return retJson.toJSONString();
        List<Map> retSmsInfoMaps = new ArrayList<>();
        for (AliyunSMSRecordInfo aliyunSMSInfo:aliyunSMSInfos) {
            Map<String,Object> smsInfoMap = new HashMap<>();
            smsInfoMap.put("smsId", aliyunSMSInfo.getId());
            smsInfoMap.put("phoneNumbers", aliyunSMSInfo.getPhoneNumbers());
            smsInfoMap.put("signName", aliyunSMSInfo.getSignName());
            smsInfoMap.put("templateCode", aliyunSMSInfo.getTemplateCode());
            smsInfoMap.put("templateParam", aliyunSMSInfo.getTemplateParam());
            smsInfoMap.put("status", aliyunSMSInfo.getStatus());
            if (aliyunSMSInfo.getStatus() == ApiCollect.ERROR) {
                smsInfoMap.put("code", aliyunSMSInfo.getCode());
                smsInfoMap.put("msg", aliyunSMSInfo.getMessage());
            }
            retSmsInfoMaps.add(smsInfoMap);
        }
        retJson.put("status", ApiCollect.SUCCESS);
        retJson.put("smsInfos", JSONObject.toJSONString(retSmsInfoMaps));
        return retJson.toJSONString();
    }

}
