package com.cqwo.base.strategy.config;

import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.appconfig.IAppConfigStrategy;
import com.cqwo.base.core.data.rdbs.repository.appconfig.AppConfigRepository;
import com.cqwo.base.core.domain.appconfig.AppConfigInfo;
import com.cqwo.base.core.errors.ApiCollect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component(value = "AppConfigStrategy")
public class AppConfigStrategy implements IAppConfigStrategy {

    @Autowired
    private AppConfigRepository appConfigRepository;

    public static Map<String, AppConfigInfo> appConfigMap = new ConcurrentHashMap<>();

    @Override
    public String registerAppConfig(String appKey, String appSecret, String packageName, Integer appType) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        AppConfigInfo appConfigInfo = new AppConfigInfo();
        appConfigInfo.setId(UUID.randomUUID().toString().replace("-",""));
        appConfigInfo.setAppKey(appKey);
        appConfigInfo.setAppSecret(appSecret);
        appConfigInfo.setPackageName(packageName);
        appConfigInfo.setAppType(appType);
        if (appConfigRepository.save(appConfigInfo) == null)
            return JSONObject.toJSONString(retMap);
        retMap.put("status", ApiCollect.SUCCESS);
        retMap.put("id", appConfigInfo.getId());
        // TODO 添加缓存
        appConfigMap.put(appConfigInfo.getId(), appConfigInfo);
        return JSONObject.toJSONString(retMap);
    }

    @Override
    public String queryAppConfigById(String id) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        // TODO 查询缓存
        //Optional<AppConfigInfo> opt  = appConfigRepository.findById(id);
        //if (!opt.isPresent()) {
        if (!appConfigMap.containsKey(id)) {
            retMap.put("msg", "无效的id，未找到对应信息");
            return JSONObject.toJSONString(retMap);
        }
        //AppConfigInfo appConfigInfo = opt.get();
        retMap.put("status", ApiCollect.SUCCESS);
        retMap.put("appInfo", appConfigMap.get(id));
        return JSONObject.toJSONString(retMap);
    }

    @Override
    public String updateAppConfigById(String id, String appKey, String appSecret, String packageName, Integer appType) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        //if (!appConfigRepository.existsById(id)) {
        if (!appConfigMap.containsKey(id)) {
            retMap.put("msg", "无效的id，未找到对应信息");
            return JSONObject.toJSONString(retMap);
        }
        appConfigRepository.deleteById(id);
        AppConfigInfo appConfigInfo = new AppConfigInfo();
        appConfigInfo.setId(id);
        appConfigInfo.setAppKey(appKey);
        appConfigInfo.setAppSecret(appSecret);
        appConfigInfo.setPackageName(packageName);
        appConfigInfo.setAppType(appType);
        appConfigRepository.save(appConfigInfo);
        // TODO 修改缓存
        appConfigMap.put(id, appConfigInfo);
        retMap.put("status", ApiCollect.SUCCESS);
        return JSONObject.toJSONString(retMap);
    }

    @Override
    public String delAppConfigById(String id) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        //if (!appConfigRepository.existsById(id)) {
        if (!appConfigMap.containsKey(id)) {
            retMap.put("msg", "无效的id，未找到对应信息");
            return JSONObject.toJSONString(retMap);
        }
        appConfigRepository.deleteById(id);
        // TODO 删除缓存
        appConfigMap.remove(id);
        retMap.put("status", ApiCollect.SUCCESS);
        return JSONObject.toJSONString(retMap);
    }

    // 初始化类
    @PostConstruct
    public void init () {
        System.out.println("启动时 我被执行了");
        List<AppConfigInfo> appConfigInfos = appConfigRepository.findAll();
        if (!appConfigInfos.isEmpty()) {
            for (AppConfigInfo a:appConfigInfos) {
                appConfigMap.put(a.getId(), a);
            }
        }
    }

    // 获取appkey and appSecret
    public static Map<String, List<AppConfigInfo>> getAppKeyAndSecret (String packageName) {
        Map<String, List<AppConfigInfo>> appsMap = new HashMap<>();
        for (Map.Entry<String, AppConfigInfo> entry : appConfigMap.entrySet()) {
            AppConfigInfo appConfigInfo = entry.getValue();
            if (appConfigInfo.getPackageName().equals(packageName)){
                List<AppConfigInfo> apps = null;
                if (appsMap.containsKey(packageName + "-" + appConfigInfo.getAppType())) {
                    apps = appsMap.get(packageName + "-" + appConfigInfo.getAppType());
                    apps.add(appConfigInfo);
                } else {
                    apps = new ArrayList<>();
                    apps.add(appConfigInfo);
                    appsMap.put(packageName + "-" + appConfigInfo.getAppType(), apps);
                }
            }
        }
        return appsMap;
    }

}
