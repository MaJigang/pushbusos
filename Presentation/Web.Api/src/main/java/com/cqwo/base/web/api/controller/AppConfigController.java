package com.cqwo.base.web.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.services.AppConfigs;
import com.cqwo.base.web.framework.controller.BaseApiController;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController(value = "ApiAppConfigController")
public class AppConfigController extends BaseApiController {

    @Resource
    private AppConfigs appConfigs;

    /**
     * 注册推送应用
     * @param appKey    应用appKey
     * @param appSecret 应用appSecret
     * @param packageName   推送使用的包名
     * @param appType   应用平台
     * @return
     */
    @PostMapping(value = {"app/register"})
    public String registerAppConfig (String appKey, String appSecret, String packageName, Integer appType) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(appKey)) {
            retMap.put("msg", "parameter appKey is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(appSecret)) {
            retMap.put("msg", "parameter appSecret is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(appType)) {
            retMap.put("msg", "parameter appType is null");
            return JSONObject.toJSONString(retMap);
        }
        return appConfigs.registerAppConfig(appKey, appSecret, packageName, appType);
    }

    /**
     * 查询推送应用
     * @param id    应用id
     * @return
     */
    @GetMapping(value = {"app/query"})
    public String queryAppConfigById (String id) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(id)) {
            retMap.put("msg", "parameter id is null");
            return JSONObject.toJSONString(retMap);
        }
        return appConfigs.queryAppConfigById(id);
    }

    /**
     * 修改
     * @param id    应用id
     * @param appKey    应用appkey
     * @param appSecret 应用appSecret
     * @param packageName   包名
     * @param appType   应用平台
     * @return
     */
    @PostMapping(value = {"app/update"})
    public String updateAppConfigById (String id, String appKey, String appSecret, String packageName, Integer appType) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(id)) {
            retMap.put("msg", "parameter id is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(appKey)) {
            retMap.put("msg", "parameter appKey is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(appSecret)) {
            retMap.put("msg", "parameter appSecret is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(appType)) {
            retMap.put("msg", "parameter appType is null");
            return JSONObject.toJSONString(retMap);
        }
        return appConfigs.updateAppConfigById(id, appKey, appSecret, packageName, appType);
    }

    /**
     * 删除应用
     * @param id    应用id
     * @return
     */
    @PostMapping(value = {"app/del"})
    public String delAppConfigById (String id) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(id)) {
            retMap.put("msg", "parameter id is null");
            return JSONObject.toJSONString(retMap);
        }
        return appConfigs.delAppConfigById(id);
    }

}
