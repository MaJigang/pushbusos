package com.cqwo.base.web.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.services.AliyunSMSes;
import com.cqwo.base.services.MobilePushs;
import com.cqwo.base.web.framework.controller.BaseApiController;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController(value = "ApiSMSController")
public class SMSController extends BaseApiController {

    @Resource
    private AliyunSMSes aliyunSMSes;

    @PostMapping(value = {"sms/send"})
    public String sendSms (String phoneNumbers, String signName, String templateCode, String templateParam) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(phoneNumbers)) {
            retMap.put("msg", "parameter phoneNumbers is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(signName)) {
            retMap.put("msg", "parameter signName is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(templateCode)) {
            retMap.put("msg", "parameter templateCode is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(templateParam)) {
            retMap.put("msg", "parameter templateParam is null");
            return JSONObject.toJSONString(retMap);
        }
        return aliyunSMSes.sendSms(phoneNumbers, signName, templateCode, templateParam);
    }

    @GetMapping(value = {"sms/query"})
    public String querySmsDetails (String smsIds) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(smsIds)) {
            retMap.put("msg", "parameter smsIds is null");
            return JSONObject.toJSONString(retMap);
        }
        return aliyunSMSes.querySmsDetails(smsIds);
    }

}
