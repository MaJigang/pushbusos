package com.cqwo.base.web.api.controller;

import com.cqwo.base.services.WeChatPushs;
import com.cqwo.base.web.framework.controller.BaseApiController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController(value = "ApiWeChatPushController")
public class WeChatPushController extends BaseApiController {

    @Resource
    private WeChatPushs weChatPushs;

    @PostMapping(value = {"wechat/sendpush"})
    public String sendPush (String appId, String openId, String templateId, String url, String miniprogram, String data) {
        return weChatPushs.sendPush(appId, openId, templateId, url, miniprogram, data);
    }

}
