package com.cqwo.base.web.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.errors.ApiCollect;
import com.cqwo.base.services.MobilePushs;
import com.cqwo.base.web.framework.controller.BaseApiController;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController(value = "ApiMobilePushController")
public class MobilePushController extends BaseApiController {

    @Resource
    private MobilePushs mobilePushs;

    /**
     * 移动推送接口
     * @param uid       用户注册id
     * @param appId     应用id
     * @param targetType    推送范围 1.广播  2.根据用户注册id推送
     * @param type      推送类型 1.通知 2.消息
     * @param title     标题
     * @param content   内容
     * @return
     */
    @PostMapping(value = {"push/send"})
    public String sendMobilePush (String uid, String appId, Integer targetType, Integer type, String title, String content,
                                  String packageName) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(uid)) {
            retMap.put("msg", "parameter uid is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(appId)) {
            retMap.put("msg", "parameter appId is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(title)) {
            retMap.put("msg", "parameter title is null");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(content)) {
            retMap.put("msg", "parameter content is null");
            return JSONObject.toJSONString(retMap);
        } else if (targetType == null) {
            retMap.put("msg", "parameter targetType is null");
            return JSONObject.toJSONString(retMap);
        } else if (targetType != 1 && targetType != 2) {
            retMap.put("msg", "parameter targetType is an error");
            return JSONObject.toJSONString(retMap);
        } else if (targetType == 2 && type == null) {
            retMap.put("msg", "parameter type is null");
            return JSONObject.toJSONString(retMap);
        } else if (type != 1 && type != 2) {
            retMap.put("msg", "parameter type is an error");
            return JSONObject.toJSONString(retMap);
        } else if (StringUtils.isEmpty(packageName)) {
            retMap.put("msg", "parameter packageName is null");
            return JSONObject.toJSONString(retMap);
        }
        Long startTime = System.currentTimeMillis();
        String ret = mobilePushs.send(uid, appId, targetType, type, title, content, packageName);
        Long time = System.currentTimeMillis() - startTime;
        System.out.println("push time : [" + time + "]");
        return ret;
    }

    /**
     * 查询移动推送接口
     * @param ids   推送id 多个以英文逗号隔开
     * @return
     */
    @GetMapping(value = {"push/query"})
    public String selMobilePushByIds (String ids) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("status", ApiCollect.ERROR);
        if (StringUtils.isEmpty(ids)) {
            retMap.put("msg", "parameter ids is null");
            return JSONObject.toJSONString(retMap);
        }
        String ret = mobilePushs.selByIds(ids);
        return ret;
    }

}
