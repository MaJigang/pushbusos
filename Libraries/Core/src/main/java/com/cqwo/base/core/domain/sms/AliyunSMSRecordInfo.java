package com.cqwo.base.core.domain.sms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_sms_record")
public class AliyunSMSRecordInfo implements Serializable {

    private static final long serialVersionUID = -7511570450540032133L;

    /**
     * 推送id 唯一标识
     */
    @Id
    @Column(name = "id", length = 40)
    private String id = "";

    /**
     * 手机号
     **/
    @Column(name = "phonenumbers", nullable = false)
    @ColumnDefault(value = "''")
    private String phoneNumbers = "";

    /**
     * 短信签名名称
     **/
    @Column(name = "signname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String signName = "";

    /**
     * 短信模板ID
     **/
    @Column(name = "templatecode", nullable = false, length = 40)
    @ColumnDefault(value = "''")
    private String templateCode = "";

    /**
     * 短信模板变量对应的实际值，JSON格式
     **/
    @Column(name = "templateparam", nullable = false)
    @ColumnDefault(value = "''")
    private String templateParam = "";

    /**
     * 发送回执ID
     **/
    @Column(name = "bizid", nullable = false, length = 40)
    @ColumnDefault(value = "''")
    private String bizId = "";

    /**
     * 请求状态码。
     **/
    @Column(name = "code", nullable = false, length = 40)
    @ColumnDefault(value = "''")
    private String code = "";

    /**
     * 状态码的描述。
     **/
    @Column(name = "message", nullable = false)
    @ColumnDefault(value = "''")
    private String message = "";

    /**
     * 请求ID。
     **/
    @Column(name = "requestid", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String requestId = "";

    /**
     * 总状态 0.失败 1.成功
     **/
    @Column(name = "status", nullable = false)
    @ColumnDefault(value = "0")
    private Integer status = 0;

    /**
     * 发送时间
     **/
    @Column(name = "createtime", nullable = false)
    private Date createTime = new Date();

}
