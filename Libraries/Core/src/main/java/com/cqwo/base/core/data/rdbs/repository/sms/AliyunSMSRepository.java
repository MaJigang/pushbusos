package com.cqwo.base.core.data.rdbs.repository.sms;

import com.cqwo.base.core.data.rdbs.repository.BaseRepository;
import com.cqwo.base.core.domain.sms.AliyunSMSRecordInfo;

public interface AliyunSMSRepository extends BaseRepository<AliyunSMSRecordInfo, String> {
}
