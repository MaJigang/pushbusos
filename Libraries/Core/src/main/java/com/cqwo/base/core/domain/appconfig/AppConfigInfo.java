package com.cqwo.base.core.domain.appconfig;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_app_config_info")
public class AppConfigInfo implements Serializable {

    private static final long serialVersionUID = -6191235174883590060L;

    /**
     * 应用ID
     */
    @Id
    @Column(name = "id", length = 40)
    private String id = ""; // 应用ID

    /**
     * app 包名
     **/
    @Column(name = "packagename", length = 40)
    @ColumnDefault(value = "''")
    private String packageName = "";

    /**
     * 应用appkey
     **/
    @Column(name = "appkey", nullable = false, length = 40)
    @ColumnDefault(value = "''")
    private String appKey = "";

    /**
     * 应用appSecret
     **/
    @Column(name = "appsecret", nullable = false, length = 40)
    @ColumnDefault(value = "''")
    private String appSecret = "";

    /**
     * 应用类型 1.阿里云 2.极光 3.友盟 4.mob 5.短信 6.微信
     **/
    @Column(name = "apptype", nullable = false)
    @ColumnDefault(value = "0")
    private Integer appType = 0;

    /**
     * 推送时间
     **/
    @Column(name = "createtime", nullable = false)
    private Date createTime = new Date();

}