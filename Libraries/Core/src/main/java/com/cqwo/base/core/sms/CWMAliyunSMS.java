package com.cqwo.base.core.sms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "CWMAliyunSMS")
public class CWMAliyunSMS {

    @Autowired(required = false)
    private IAliyunSMSStrategy iAliyunSMSStrategy;

    public IAliyunSMSStrategy getiAliyunSMSStrategy() {
        return iAliyunSMSStrategy;
    }

    public void setiAliyunSMSStrategy(IAliyunSMSStrategy iAliyunSMSStrategy) {
        this.iAliyunSMSStrategy = iAliyunSMSStrategy;
    }
}
