package com.cqwo.base.core.appconfig;

public interface IAppConfigStrategy {

    /**
     * 注册推送应用
     * @param appKey    应用appKey
     * @param appSecret 应用appSecret
     * @param packageName   推送使用的包名
     * @param appType   应用平台
     * @return
     */
    String registerAppConfig(String appKey, String appSecret, String packageName, Integer appType);

    /**
     * 查询推送应用
     * @param id    应用id
     * @return
     */
    String queryAppConfigById (String id);

    /**
     * 修改
     * @param id    应用id
     * @param appKey    应用appkey
     * @param appSecret 应用appSecret
     * @param packageName   包名
     * @param appType   应用平台
     * @return
     */
    String updateAppConfigById (String id, String appKey, String appSecret, String packageName, Integer appType);

    /**
     * 删除应用
     * @param id    应用id
     * @return
     */
    String delAppConfigById (String id);

}
