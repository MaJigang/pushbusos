package com.cqwo.base.core.push;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CWMWeChatPush")
public class CWMWeChatPush {

    @Autowired( required = false )
    private IWeChatPushStrategy iWeChatPushStrategy;

    public IWeChatPushStrategy getiWeChatPushStrategy() {
        return iWeChatPushStrategy;
    }

    public void setiWeChatPushStrategy(IWeChatPushStrategy iWeChatPushStrategy) {
        this.iWeChatPushStrategy = iWeChatPushStrategy;
    }
}
