package com.cqwo.base.core.sms;

public interface IAliyunSMSStrategy {

    /**
     * 发送阿里云短信
     * @param phoneNumbers  接收短信的手机号码
     * @param signName      短信签名名称
     * @param templateCode  短信模板ID
     * @param templateParam 短信模板变量对应的实际值，JSON格式
     * @return
     */
    String sendSms (String phoneNumbers, String signName, String templateCode, String templateParam);

    /**
     * 查询短信详情
     * @param smsIds    短信唯一标识ID 多个以英文逗号分隔
     * @return
     */
    String querySmsDetails (String smsIds);

}
