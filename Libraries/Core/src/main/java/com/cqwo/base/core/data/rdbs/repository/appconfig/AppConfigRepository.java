package com.cqwo.base.core.data.rdbs.repository.appconfig;

import com.cqwo.base.core.data.rdbs.repository.BaseRepository;
import com.cqwo.base.core.domain.appconfig.AppConfigInfo;

public interface AppConfigRepository extends BaseRepository<AppConfigInfo, String> {
}
