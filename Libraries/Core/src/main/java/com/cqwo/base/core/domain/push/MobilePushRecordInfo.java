package com.cqwo.base.core.domain.push;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_mobile_push_record")
public class MobilePushRecordInfo implements Serializable {

    private static final long serialVersionUID = -3571192280756826922L;

    /**
     * 推送id 唯一标识
     */
    @Id
    @Column(name = "id", length = 40)
    private String id = "";

    /**
     * 用户id
     **/
    @Column(name = "uid", nullable = false, length = 1024)
    @ColumnDefault(value = "''")
    private String uid = "";

    /**
     * 应用id (用于区分各系统应用)
     **/
    @Column(name = "appid", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String appId = "";

    /**
     * 推送范围 1.广播    2.根据注册id推送
     **/
    @Column(name = "targettype", nullable = false)
    @ColumnDefault(value = "0")
    private Integer targetType = 0;

    /**
     * 推送类型 1.通知 2.消息
     **/
    @Column(name = "type", nullable = false)
    @ColumnDefault(value = "0")
    private Integer type = 0;

    /**
     * 内容
     **/
    @Column(name = "content  ", nullable = false, length = 1024)
    @ColumnDefault(value = "''")
    private String content   = "";

    /**
     * 标题
     **/
    @Column(name = "title", nullable = false)
    @ColumnDefault(value = "''")
    private String title = "";

    /**
     * 总状态 0.失败 1.成功
     **/
    @Column(name = "status", nullable = false)
    @ColumnDefault(value = "0")
    private Integer status = 0;

    /**
     * 阿里云推送返回信息
     **/
    @Column(name = "apushretvalue", nullable = false)
    @ColumnDefault(value = "''")
    private String apushRetValue = "";

    /**
     * 极光推送返回信息
     **/
    @Column(name = "jpushretvalue", nullable = false)
    @ColumnDefault(value = "''")
    private String jpushRetValue = "";

    /**
     * mob推送返回信息
     **/
    @Column(name = "mpushretvalue", nullable = false)
    @ColumnDefault(value = "''")
    private String mpushRetValue = "";

    /**
     * umeng推送返回信息
     **/
    @Column(name = "upushretvalue", nullable = false)
    @ColumnDefault(value = "''")
    private String upushRetValue = "";

    /**
     * 移动端包名
     **/
    @Column(name = "packagename", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String packageName = "";

    /**
     * 推送时间
     **/
    @Column(name = "createtime", nullable = false)
    private Date createTime = new Date();

}
