package com.cqwo.base.core.push;

public interface IWeChatPushStrategy {

    /**
     * 微信推送
     * @param appId     // 应用唯一标识ID
     * @param openId    // 接收者openid
     * @param templateId    // 模板ID
     * @param url   // 模板跳转链接
     * @param miniprogram   // 跳小程序所需数据
     * @param data  // 模板数据
     * @return
     */
    String sendPush (String appId, String openId, String templateId, String url, String miniprogram, String data);

}
