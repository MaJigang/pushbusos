package com.cqwo.base.core.appconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CWMAppConfig")
public class CWMAppConfig {

    @Autowired( required = false )
    private IAppConfigStrategy iAppConfigStrategy;

    public IAppConfigStrategy getiAppConfigStrategy() {
        return iAppConfigStrategy;
    }

    public void setiAppConfigStrategy(IAppConfigStrategy iAppConfigStrategy) {
        this.iAppConfigStrategy = iAppConfigStrategy;
    }
}
