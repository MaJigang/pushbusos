package com.cqwo.base.core.domain.push;

import com.cqwo.base.core.errors.ApiCollect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_wechat_push_record")
public class WeChatPushRecordInfo implements Serializable {

    private static final long serialVersionUID = 3868173561948175500L;

    /**
     * 推送id 唯一标识
     */
    @Id
    @Column(name = "id", length = 40)
    private String id = "";

    /**
     * 应用id
     **/
    @Column(name = "appid", nullable = false)
    @ColumnDefault(value = "''")
    private String appId = "";


    /**
     * 接收者openid
     **/
    @Column(name = "openid", nullable = false)
    @ColumnDefault(value = "''")
    private String openId = "";

    /**
     * 模板ID
     **/
    @Column(name = "templateid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String templateId = "";

    /**
     * 模板跳转链接
     **/
    @Column(name = "url", nullable = false)
    @ColumnDefault(value = "''")
    private String url = "";

    /**
     * 跳小程序所需数据
     **/
    @Column(name = "miniprogram", nullable = false)
    @ColumnDefault(value = "''")
    private String miniprogram = "";

    /**
     * 模板数据
     **/
    @Column(name = "data", nullable = false)
    @ColumnDefault(value = "''")
    private String data = "";

    /**
     * 平台返回状态
     **/
    @Column(name = "errcode", nullable = false)
    private Integer errCode = 1;

    /**
     * 平台返回状态信息
     **/
    @Column(name = "errmsg", nullable = false)
    @ColumnDefault(value = "''")
    private String errMsg = "";

    /**
     * 平台返回id
     **/
    @Column(name = "msgid", nullable = false)
    @ColumnDefault(value = "''")
    private String msgId = "";

    /**
     * 总状态
     **/
    @Column(name = "status", nullable = false)
    private Integer status = ApiCollect.ERROR;

    /**
     * 推送时间
     **/
    @Column(name = "createtime", nullable = false)
    private Date createTime = new Date();

}
