package com.cqwo.base.core.push;

public interface IMobilePushStrategy {

    /**
     * 发送推送消息
     * @param uid   用户注册id 多个时英文逗号隔开
     * @param appId 应用id
     * @param targetType    推送范围
     * @param type      推送类型
     * @param title     标题
     * @param content   内容
     * @return
     */
    String sendPush (String uid, String appId, Integer targetType, Integer type, String title, String content,
                     String packageName);

    /**
     * 查看推送详情信息
     * @param pushIds    推送id    多个时英文逗号隔开
     * @return
     */
    String selPushByIds (String pushIds);

}
