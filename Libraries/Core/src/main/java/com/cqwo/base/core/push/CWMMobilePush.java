package com.cqwo.base.core.push;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CWMMobilePush")
public class CWMMobilePush {

    @Autowired( required = false )
    private IMobilePushStrategy iMobilePushStrategy;

    public IMobilePushStrategy getiMobilePushStrategy() {
        return iMobilePushStrategy;
    }

    public void setiMobilePushStrategy(IMobilePushStrategy iMobilePushStrategy) {
        this.iMobilePushStrategy = iMobilePushStrategy;
    }
}
