package com.cqwo.base.core.data.rdbs.repository.push;

import com.cqwo.base.core.data.rdbs.repository.BaseRepository;
import com.cqwo.base.core.domain.push.MobilePushRecordInfo;

public interface MobilePushRepository extends BaseRepository<MobilePushRecordInfo, String> {

}
