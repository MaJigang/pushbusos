package com.cqwo.base.services;

import com.cqwo.base.core.sms.CWMAliyunSMS;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service(value = "AliyunSMSes")
public class AliyunSMSes {

    @Resource
    private CWMAliyunSMS cwmAliyunSMS;

    public String sendSms (String phoneNumbers, String signName, String templateCode, String templateParam) {
       return cwmAliyunSMS.getiAliyunSMSStrategy().sendSms(phoneNumbers, signName, templateCode, templateParam);
    }

    public String querySmsDetails (String smsIds) {
        return cwmAliyunSMS.getiAliyunSMSStrategy().querySmsDetails(smsIds);
    }

}
