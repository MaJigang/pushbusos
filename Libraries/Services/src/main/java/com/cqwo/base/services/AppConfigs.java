package com.cqwo.base.services;

import com.cqwo.base.core.appconfig.CWMAppConfig;
import com.cqwo.base.core.domain.appconfig.AppConfigInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service(value = "AppConfigs")
public class AppConfigs {

    @Resource
    private CWMAppConfig cwmAppConfig;

    public String registerAppConfig (String appKey, String appSecret, String packageName, Integer appType) {
        return cwmAppConfig.getiAppConfigStrategy().registerAppConfig(appKey, appSecret, packageName, appType);
    }

    public String queryAppConfigById (String id) {
        return cwmAppConfig.getiAppConfigStrategy().queryAppConfigById(id);
    }

    public String updateAppConfigById (String id, String appKey, String appSecret, String packageName, Integer appType) {
        return cwmAppConfig.getiAppConfigStrategy().updateAppConfigById(id, appKey, appSecret, packageName, appType);
    }

    public String delAppConfigById (String id) {
        return cwmAppConfig.getiAppConfigStrategy().delAppConfigById(id);
    }

}
