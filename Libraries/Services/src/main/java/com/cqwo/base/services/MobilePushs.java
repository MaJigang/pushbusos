package com.cqwo.base.services;

import com.alibaba.fastjson.JSONObject;
import com.cqwo.base.core.push.CWMMobilePush;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service(value = "MobilePushs")
public class MobilePushs {

    @Resource
    private CWMMobilePush cwmMobilePush;

    public String send (String uId, String appId, Integer targetType, Integer type, String title, String content,
                        String packageName) {
        return cwmMobilePush.getiMobilePushStrategy().sendPush(uId, appId, targetType, type, title, content, packageName);
    }

    public String selByIds (String ids) {
        return cwmMobilePush.getiMobilePushStrategy().selPushByIds(ids);
    }

}
