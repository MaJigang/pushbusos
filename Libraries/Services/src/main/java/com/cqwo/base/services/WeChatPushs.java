package com.cqwo.base.services;

import com.cqwo.base.core.push.CWMWeChatPush;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service(value = "WeChatPushs")
public class WeChatPushs {

    @Resource
    private CWMWeChatPush cwmWeChatPush;

    public String sendPush (String appId, String openId, String templateId, String url, String miniprogram, String data) {
        return cwmWeChatPush.getiWeChatPushStrategy().sendPush(appId, openId, templateId, url, miniprogram, data);
    }

}
